﻿using WcfServicioJSON.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ServiceSample
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "PersonaService" en el código, en svc y en el archivo de configuración a la vez.
    // NOTA: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione PersonaService.svc o PersonaService.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class PersonaService : IPersonaService
    {
        public List<Persona> SelectPersona()
        {
            var list = new List<Persona>();
            using (var db = new MERJsonContainer())
            {
                
                list = db.PersonaSet.ToList();
            }

            return list;
        }

        public Persona InsertPersona(Persona model)
        {
            using (var db = new MERJsonContainer())
            {
                db.PersonaSet.Add(model);

                if (db.ChangeTracker.HasChanges())
                {
                    db.SaveChanges();
                }
            }

            return model;
        }

        public Persona UpdatePersona(int id, Persona model)
        {
            throw new NotImplementedException();
        }

        public void DeletePersona(int id)
        {
            using (var db = new MERJsonContainer())
            {
                Persona persona = db.PersonaSet.Single(x => x.Id == id);
                db.PersonaSet.Remove(persona);

                if (db.ChangeTracker.HasChanges())
                {
                    db.SaveChanges();
                }
            }
        }
    }
}
