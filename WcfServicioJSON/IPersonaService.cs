﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using WcfServicioJSON.Model;

namespace ServiceSample
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IPersonaService" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface IPersonaService
    {
        [OperationContract]
        List<Persona> SelectPersona();

        [OperationContract]
        Persona InsertPersona(Persona model);

        [OperationContract]
        Persona UpdatePersona(int id, Persona model);

        [OperationContract]
        void DeletePersona(int id);
    }
}
